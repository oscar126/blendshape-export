using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;


using UnityEngine;
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class ExportBlendshapeValues : MonoBehaviour
{


  List<Vector3> _diff;

  [SerializeField] GameObject _gameObjectA;
  [SerializeField] GameObject _gameObjectB;


  

  //public MeshFilter _canonicalMesh;
  Mesh _meshA, _meshB;
  List<Vector3> _verts;
  SkinnedMeshRenderer _smrA, _smrB;

  [SerializeField]  int[] _steps;


  [SerializeField]  int[] _blendshapeIndices;



  [SerializeField] int[] _blendshapeIndicesRandomized;


  int [][] permutationTable;
  int _currentPermutation=0;

  List<string> _lines;

  [SerializeField] string _filename;

  


  [SerializeField] Transform debug;
  [SerializeField] int   _debugIndex = 0;




   readonly static int _vertTop = 10;
   readonly static int _vertBot = 152;

   readonly static int _vertLeft=234;
   readonly static int _vertRight=454;

   readonly static int _vertFront=1;


  //private string   _previous = "";
   //private string  _inputs = "";
   //private string _outputs = "";



  

  // Start is called before the first frame update
  public  void Start()
  {
    Application.runInBackground = true;
    _lines = new List <string>();
    _smrA = _gameObjectA.GetComponent<SkinnedMeshRenderer>();

    if ( _gameObjectB!=null)   _smrB = _gameObjectB.GetComponent<SkinnedMeshRenderer>();

   
    float[] blendshapeVals = new float[_blendshapeIndices.Count()];
 

    int[][] perminmputarr =  new int [_blendshapeIndices.Count()][];
    for (int i = 0; i<_blendshapeIndices.Count(); i++)
    {
      perminmputarr[i] = _steps;
    }

    permutationTable = Permutations(perminmputarr);
   



    var ma = new Mesh();   _smrA.BakeMesh(ma);
    var mb = new Mesh();
    if ( _gameObjectB!=null) {
      
        _smrB.BakeMesh(mb);
      }




    GetComponent<MeshFilter>().mesh = new Mesh();

    List<Vector3> verts = new List<Vector3>();
    ma.GetVertices(verts);
    GetComponent<MeshFilter>().mesh.SetVertices(verts);


    
    var tris = ma.GetTriangles(0);
    GetComponent<MeshFilter>().mesh.SetTriangles(tris,0);

    var norms =  new List<Vector3>();
    ma.GetNormals(norms);
    GetComponent<MeshFilter>().mesh.SetNormals(norms);

    GetComponent<MeshFilter>().mesh.RecalculateBounds();
    GetComponent<MeshFilter>().mesh.RecalculateNormals();
    GetComponent<MeshFilter>().mesh.MarkModified();


    if ( _gameObjectB!=null) 
    {
      _diff = new List<Vector3>();
      for (int i=0; i<ma.vertexCount;i++)
      {
        var v = mb.vertices[i]-ma.vertices[i];    
        _diff.Add(v);
      }
    }



  }


 public void LateUpdate(){

      if (_currentPermutation%500==0 || _currentPermutation==permutationTable.Length-1)
      {
        Debug.Log((_currentPermutation+1).ToString()+" / "+ permutationTable.Length.ToString());
        System.IO.File.AppendAllLines(@_filename, _lines);
        _lines.Clear();
      }


 }

  public  void Update()
  {

    

    if (_currentPermutation<permutationTable.Length){
       for (int s=0; s<permutationTable[_currentPermutation].Length; s++)
       {
           _smrA.SetBlendShapeWeight(  _blendshapeIndices[s], permutationTable[_currentPermutation][s]);
          if ( _gameObjectB!=null)  _smrB.SetBlendShapeWeight(  _blendshapeIndices[s], permutationTable[_currentPermutation][s]);
       }


      for (int s=0; s<_blendshapeIndicesRandomized.Length; s++)
       {
          var rand = Random.value;
           _smrA.SetBlendShapeWeight( _blendshapeIndicesRandomized[s], rand*100);
           if ( _gameObjectB!=null)  _smrB.SetBlendShapeWeight( _blendshapeIndicesRandomized[s], rand*100);
       }





      

      
      string lineA = "";

      for (int  i = 0; i<_blendshapeIndices.Count(); i++)
      {
        lineA+=permutationTable[_currentPermutation][i].ToString()+" ";
      }

      //augment
      var rV = new Vector3(Random.value,Random.value,Random.value);

      //augment scale
      float sVxy = (float)(1.0 - Random.value * 0.1f); //0.9-1
      float sVz = (float)(1.0 - Random.value* 0.3f); //0.7 -1 cause skewing when rotating face

      var sV = new Vector3(sVxy,sVxy, sVz);


      //store blendshape values

      //store vertex values
      var ma = new Mesh();
      //var mb = new Mesh();
      _smrA.BakeMesh(ma);


      //_smrB.BakeMesh(mb);
      
      var verts = new List<Vector3>();
      ma.GetVertices(verts);

      if  ( _gameObjectB!=null) 
      {
        for (int i =0; i<verts.Count;i++)
        {
          verts[i]= verts[i]+Vector3.Scale(_diff[i],rV);
        
        }  
       


       verts = CenterNormalizeFlip(verts);

     
       for (int i =0; i<verts.Count;i++)
        {
          verts[i]= Vector3.Scale(verts[i],sV);
        
        }  
      }


      GetComponent<MeshFilter>().mesh.vertices = verts.ToArray(); //preview


      
      
      string lineB ="";
      for (int i = 0; i < verts.Count; i++)
      {
        lineB += verts[i].x.ToString() + " " +verts[i].y.ToString() + " " + verts[i].z.ToString() + ", ";
      }

      _lines.Add(lineA);
      _lines.Add(lineB);



      _currentPermutation++;

    }
      
  }

  static bool isHigher(Vector3 a, Vector3 b) => a.y>b.y ?true:false;
  static bool isToTheLeft(Vector3 a, Vector3 b) =>a.x<b.x? true:false;
  static bool isFrontToBack(Vector3 a, Vector3 b)=>a.z<b.z? true:false;
  public static List<Vector3> CenterNormalizeFlip(List<Vector3> verts)
  {
      var normY = Mathf.Abs(verts[_vertTop].y - verts[_vertBot].y);
      var normX =Mathf.Abs(verts[_vertLeft].x - verts[_vertRight].x);
      var normZ =  Mathf.Abs(verts[_vertFront].z - Mathf.Min(verts[_vertRight].z,verts[_vertLeft].z));
      var norm = (verts[_vertTop] - verts[_vertBot]).magnitude;


      var v0 = verts[0];

     
       for (var vi=0;vi<verts.Count;vi++)
       {
          //center on 0
           verts[vi] =verts[vi] - v0; 


          //scale to cube
          verts[vi] = Vector3.Scale(verts[vi],new Vector3(1, normX/normY, normX/normZ) );
          //normalize
          verts[vi] = verts[vi]/norm;

        
       }





       //flip Y if need
       if (isHigher(verts[_vertBot],verts[_vertTop]))
       {
          for (var vi=0;vi<verts.Count;vi++)
          {
              verts[vi] = new Vector3(verts[vi].x,  -verts[vi].y, verts[vi].z );
          }     
       }

      //flip X if need
       if (isToTheLeft(verts[_vertLeft], verts[_vertRight]))
       {
          for (var vi=0;vi<verts.Count;vi++)
          {
              verts[vi] = new Vector3(-verts[vi].x,  verts[vi].y, verts[vi].z );
          }     
       }

      
    //normalize
/*
    for (var vi=0; vi<verts.Count; vi++)
    {
      verts[vi] = new Vector3(verts[vi].x / normY, verts[vi].y / normY, verts[vi].z / normY);
    }
*/






  
   

    return verts;


  }





 

  public T[][] Permutations<T>(T[][] vals)
  {
    int numCols = vals.Length;
    int numRows = vals.Aggregate(1, (a, b) => a * b.Length);

    var results = Enumerable.Range(0, numRows)
                                        .Select(c => new T[numCols])
                                        .ToArray();

    int repeatFactor = 1;
    for(int c = 0; c < numCols; c++)
    {
      for(int r = 0; r < numRows; r++)
        results[   r   ][c] = vals[c][   r / repeatFactor % vals[c].Length];
      repeatFactor *= vals[c].Length;
    }

    return results;
  }




}
