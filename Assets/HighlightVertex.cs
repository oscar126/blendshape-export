using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightVertex : MonoBehaviour
{

    [SerializeField] private int _vertInd = 0;
    [SerializeField] Vector3 _vertexCoords;
    [SerializeField] private Transform _marker;

    MeshFilter _mfilter;
    Mesh _m;
    // Start is called before the first frame update
    void Start()
    {



        _mfilter = GetComponent<MeshFilter>();
        if (_mfilter ==null){
            _m = new Mesh();
            var mfilter = GetComponent<SkinnedMeshRenderer>();
            mfilter.BakeMesh(_m);
            Debug.Log(gameObject.name+": "+_m.vertexCount+" vertices");
        }else{
            Debug.Log(gameObject.name+": "+_mfilter.mesh.vertexCount+" vertices");

        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp("left"))
        {
          _vertInd--;
        
        }

        if (Input.GetKeyUp("right"))
        {

            _vertInd++;
         

        }



         
            if (_mfilter!=null){

                 if (_vertInd>=0 && _vertInd<_mfilter.mesh.vertexCount )  {
                     _marker.position = transform.TransformPoint(_mfilter.mesh.vertices[_vertInd]);
                      _vertexCoords = _mfilter.mesh.vertices[_vertInd];
                
                 }
            }
            if (_m!=null){
                 if (_vertInd>=0 && _vertInd<_m.vertexCount ){ 
                     _marker.position = transform.TransformPoint(_m.vertices[_vertInd]);
                      _vertexCoords = _m.vertices[_vertInd];
                     }

            }

           
          

    }
}
