using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Barracuda;
using System.Runtime.InteropServices;

public class EstimateBlendshape : MonoBehaviour
{
      [DllImport ("callTorch")] static extern void initTorch(int a, int b);  
      [DllImport ("callTorch")] static extern void GetBlendshape(ref float inputBuffer, ref float outputBuffer);
      Model _model;
      Mesh inputMesh;

      IWorker _worker;

      List<Vector3> _meshlist;
      [SerializeField] SkinnedMeshRenderer outputBlendshapes;
    //[SerializeField] int multiplier = 100; //tyle powinno byc

        //mouth
   [SerializeField] int [] _activeVerts;// ={10, 11, 12, 13, 14, 15, 16, 17, 20, 21, 22, 54, 62, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 186, 192, 195, 198, 199, 202, 206, 246, 248, 250, 260, 315, 316, 320, 321, 351, 374, 393, 395, 396, 397, 398, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 444, 450, 451, 453, 454, 455, 456, 457, 458, 459, 460, 462, 463, 464, 465, 466, 467};

    [SerializeField] int[] _blendshapeIndices;

    float [] copyVector3ArrtoFloatArr(List<Vector3> input, int [] indices=null){
     
        if (indices==null){
            var result = new float[3*input.Count];
            for (int i = 0; i< input.Count; i+=3)
            {
               
                result[i]= input[i].x;
                result[i+1]= input[i].y;
                result[i+2]= input[i].z;



            }


            return result;
        }else{
            var result = new float[3*indices.Length];
            for (int i = 0; i< result.Length; i+=3)
            {

                Debug.Log(i);
                if (i==495){
                        int tttt = 5;

                }
                result[i]  = input[indices[i/3]].x;
                result[i+1]= input[indices[i/3]].y;
                result[i+2]= input[indices[i/3]].z;



            }

            return result;


        }
    }

    // Start is called before the first frame update
    void Start()
    {

        _activeVerts  = new int[] {0, 2, 11, 12, 13, 14, 15, 16, 17, 18, 20, 32, 36, 37, 38, 39, 40, 41, 42, 43, 48, 49, 50, 57, 59, 60, 61, 62, 64, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 94, 95, 96, 97, 98, 99, 102, 106, 123, 129, 135, 136, 138, 140, 141, 142, 146, 147, 148, 149, 150, 152, 164, 165, 166, 167, 169, 170, 171, 175, 176, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 191, 192, 194, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 210, 211, 212, 213, 214, 216, 218, 219, 235, 238, 240, 242, 250, 262, 266, 267, 268, 269, 270, 271, 272, 273, 278, 279, 280, 287, 289, 290, 291, 292, 294, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 324, 325, 326, 327, 328, 331, 335, 352, 358, 364, 365, 367, 369, 370, 371, 375, 376, 377, 378, 379, 391, 392, 393, 394, 395, 396, 400, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 415, 416, 418, 421, 422, 423, 424, 425, 426, 427, 428, 430, 431, 432, 433, 434, 436, 438, 439, 455, 458, 460, 462};



        
        // {10, 11, 12, 13, 14, 15, 16, 17, 20, 21, 22, 54, 62, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 186, 192, 195, 198, 199, 202, 206, 246, 248, 250, 260, 315, 316, 320, 321, 351, 374, 393, 395, 396, 397, 398, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 444, 450, 451, 453, 454, 455, 456, 457, 458, 459, 460, 462, 463, 464, 465, 466, 467};

       //Debug.Log(activeVerts.Length);
       initTorch(_activeVerts.Length*3, _blendshapeIndices.Length);
       
      inputMesh =  new Mesh(); // GetComponent<SkinnedMeshRenderer>().sharedMesh;
      _meshlist = new List<Vector3>();
    }






    // Update is called once per frame
   

    void Update()
    {

        
        GetComponent<SkinnedMeshRenderer>().BakeMesh(inputMesh);
        inputMesh.GetVertices(_meshlist);

        _meshlist = ExportBlendshapeValues.CenterNormalizeFlip(_meshlist);

        var input = copyVector3ArrtoFloatArr (_meshlist,_activeVerts);
        HelperScriptsForDebugging.SaveVector3ToCSV(_meshlist.ToArray(), "d://DEV//output//all.csv" );

        HelperScriptsForDebugging.SaveArray1DtoCSV(input,"d://DEV//output//selected.csv");
        
        
        var output = new float[_blendshapeIndices.Length];


        GetBlendshape(ref input[0], ref output[0]);


        
        for (int i =0; i<_blendshapeIndices.Length;i++){
                outputBlendshapes.SetBlendShapeWeight(_blendshapeIndices[i], output[i]*100);
                Debug.Log(i.ToString()+": "+output[i]*100);
        }
        



        //var inputV3  = HelperScriptsForDebugging.LoadVector3FromCSV("D:\\DEV\\variant0.csv");
       // var input = copyVector3ArrtoFloatArr (inputV3);




/*

        //var inputTensor = new Tensor(1, 212*3, input);
        var inputTensor = new Tensor( new int[]{1,1,636,1,1,1,1,1}, input,"",true);
           
        _worker.Execute(inputTensor);

        var output = _worker.PeekOutput();

        if (output.length == _blendshapeIndices.Length){
            for (int i =0; i< output.length;i++)
                outputBlendshapes.SetBlendShapeWeight(_blendshapeIndices[i], output[i]*100);

        }
       
  */    

        
    }


     private void OnDisable() {
        _worker.Dispose();
  

    }
}
